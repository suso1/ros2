﻿#include <chrono>
#include <functional>
#include <memory>
#include <string>

#include "driver_msg/msg/idas_data.hpp"
#include "move_control_msgs/msg/teleop.hpp"
#include "rclcpp/rclcpp.hpp"

using std::placeholders::_1;

class TeleopMotor : public rclcpp::Node {
public:
  TeleopMotor() : Node("joy_control") {
    this->declare_parameter<double>("scale_linear_x", 1.2);
    this->get_parameter("scale_linear_x", l_scale_x_);
    RCLCPP_INFO(this->get_logger(), "scale_linear_x = '%f'", l_scale_x_);

    this->declare_parameter<double>("scale_linear_y", 1.2);
    this->get_parameter("scale_linear_y", l_scale_y_);
    RCLCPP_INFO(this->get_logger(), "scale_linear_y = '%f'", l_scale_y_);

    this->declare_parameter<double>("scale_ang_z", 1.2);
    this->get_parameter("scale_ang_z", a_scale_z_);
    RCLCPP_INFO(this->get_logger(), "scale_ang_z = '%f'", a_scale_z_);

    this->declare_parameter<double>("speed_scale", 1.0);
    this->get_parameter("speed_scale", speed_scale_);
    RCLCPP_INFO(this->get_logger(), "speed_scale = '%f'", speed_scale_);

    this->declare_parameter<double>("slow_scale", 0.5);
    this->get_parameter("slow_scale_;", slow_scale_);
    RCLCPP_INFO(this->get_logger(), "slow_scale_; = '%f'", slow_scale_);

    pub_joy_ = this->create_publisher<move_control_msgs::msg::Teleop>(
        "/cmd_vel_joy", 1);
    sub_idas_ = this->create_subscription<driver_msg::msg::IdasData>(
      "/idas_data", 10, std::bind(&TeleopMotor::idas_callback, this, _1));
  }
  /*
  int run() {

    while (rclcpp::ok()) {
      publish_motor_command(vel_x_, vel_y_, ang_z_);
      rclcpp::sleep_for(std::chrono::milliseconds(10));
    }
    publish_motor_command(0, 0, 0);
    return 0;
  }
  */

private:
  rclcpp::Publisher<move_control_msgs::msg::Teleop>::SharedPtr pub_joy_;
  rclcpp::Subscription<driver_msg::msg::IdasData>::SharedPtr sub_idas_;

  void idas_callback(const driver_msg::msg::IdasData::SharedPtr idas) {
    RCLCPP_INFO(this->get_logger(), "Receiving Idas signal");
    if (idas->da3 == 123 && idas->da4 == 2 && idas->da2 == 64) 
    {
      RCLCPP_INFO(this->get_logger(), "Idas active");
      if (idas->ky1 == 66) 
      {
        RCLCPP_INFO(this->get_logger(), "Mode enable");
        if(idas->ky4 == 36 || idas->ky4 == 37 || idas->ky4 == 38)
        {
          agv_mode_ = 0;
          robot_arm_mode_ = 1;
        }
        else if(idas->ky4 == 24 || idas->ky4 == 25 || idas->ky4 == 26)
        {
          agv_mode_ = 1;
          RCLCPP_INFO(this->get_logger(), "AGV Manual mode");
          robot_arm_mode_ = 0;
          if(idas->ky4 == 25)
            agv_mode_ = 2;
          else if(idas->ky4 == 26)
            agv_mode_ = 3;
        }
        else if(idas->ky4 == 40 || idas->ky4 == 41 || idas->ky4 == 42)
        {
          agv_mode_ = 1;
          RCLCPP_INFO(this->get_logger(), "AGV Manual mode");
          robot_arm_mode_ = 1;
          if(idas->ky4 == 41)
            agv_mode_ = 2;
          else if(idas->ky4 == 42)
            agv_mode_ = 3;
        }
        else
        {
          agv_mode_ = 0;
          robot_arm_mode_ = 0;
        }

      } 
      else 
      {
        agv_mode_ = -3;
        robot_arm_mode_ = 0;
      }
    } 
    else 
    {
      agv_mode_ = -3;
      robot_arm_mode_ = 0;
    }

    double lin_spd_x = 0, lin_spd_y = 0, rot_spd_z = 0;
    int dir_x = 0, dir_y = 0, dir_z = 0;

    RCLCPP_INFO(this->get_logger(), "agvmode = '%d', an3 = '%f', an4 = '%f'",agv_mode_, idas->an3, idas->an4);

    switch (agv_mode_) 
    {
    case 1: {
      if (fabs(idas->an3) >= 0.1 && fabs(idas->an4) < 0.1) {
        lin_spd_x = double(fabs(idas->an3) - 0.1);
        lin_spd_y = 0;
        RCLCPP_INFO(this->get_logger(), "velx = '%f'",lin_spd_x);
      } else {
        if (fabs(idas->an3) < 0.1 && fabs(idas->an4) >= 0.1) {
          lin_spd_x = 0;
          lin_spd_y = double(fabs(idas->an4) - 0.1);
          RCLCPP_INFO(this->get_logger(), "vely = '%f'",lin_spd_y);
        } else {
          if (fabs(idas->an3) >= 0.1 && fabs(idas->an4) >= 0.1) {
            lin_spd_x = double(fabs(idas->an3) - 0.1);
            lin_spd_y = double(fabs(idas->an4) - 0.1);
          } else {
            lin_spd_x = 0;
            lin_spd_y = 0;
          }
        }
      }
      rot_spd_z = 0.3 * fabs(idas->an2);
    } break;
    case 2: {
      if (fabs(idas->an3) >= 0.1 && fabs(idas->an4) < 0.1) {
        lin_spd_x = double(fabs(idas->an3) - 0.1) * slow_scale_;
        //lin_spd_x = idas->an3;
        RCLCPP_INFO(this->get_logger(), "velx = '%f'",lin_spd_x);
        lin_spd_y = 0;
      } else {
        if (fabs(idas->an3) < 0.1 && fabs(idas->an4) >= 0.1) {
          lin_spd_x = 0;
          lin_spd_y = double(fabs(idas->an4) - 0.1) * slow_scale_;
          //lin_spd_y = idas->an4;
          RCLCPP_INFO(this->get_logger(), "vely = '%f'",lin_spd_y);
        } else {
          if (fabs(idas->an3) >= 0.1 && fabs(idas->an4) >= 0.1) {
            lin_spd_x = double(fabs(idas->an3) - 0.1) * slow_scale_;
            lin_spd_y = double(fabs(idas->an4) - 0.1) * slow_scale_;
            //lin_spd_x = idas->an3;
            //lin_spd_y = idas->an4;
            RCLCPP_INFO(this->get_logger(), "velx = '%f', vely = '%f'",lin_spd_x, lin_spd_y);
          } else {
            lin_spd_x = 0;
            lin_spd_y = 0;
          }
        }
      }
      //rot_spd_z = 0.3 * fabs(idas->an2) * slow_scale_;
      rot_spd_z = 0.3 * idas->an2;
    } break;
    case 3: {
      if (fabs(idas->an3) >= 0.1 && fabs(idas->an4) < 0.1) {
        lin_spd_x = double(fabs(idas->an3) - 0.1) * l_scale_x_;
        lin_spd_y = 0;
      } else {
        if (fabs(idas->an3) < 0.1 && fabs(idas->an4) >= 0.1) {
          lin_spd_x = 0;
          lin_spd_y = double(fabs(idas->an4) - 0.1) * l_scale_y_;
        } else {
          if (fabs(idas->an3) >= 0.1 && fabs(idas->an4) >= 0.1) {
            lin_spd_x = double(fabs(idas->an3) - 0.1) * l_scale_x_;
            lin_spd_y = double(fabs(idas->an4) - 0.1) * l_scale_y_;
          } else {
            lin_spd_x = 0;
            lin_spd_y = 0;
          }
        }
      }
      rot_spd_z = 0.3 * fabs(idas->an2) * a_scale_z_;
    } break;
    case 0: {
      lin_spd_x = 0;
      lin_spd_y = 0;
      rot_spd_z = 0;

    } break;
    default: {
      lin_spd_x = 0;
      lin_spd_y = 0;
      rot_spd_z = 0;
    } break;
    }

    dir_x = sgn(idas->an3);
    dir_y = sgn(idas->an4);
    dir_z = sgn(idas->an2);

    vel_x_ = double(lin_spd_x * dir_x * speed_scale_);
    vel_y_ = double(lin_spd_y * dir_y * speed_scale_);
    ang_z_ = double(rot_spd_z * dir_z * speed_scale_);

    //vel_x_ = -lin_spd_x;
    //vel_y_ = -lin_spd_y;
    //ang_z_ = -rot_spd_z;

    publish_motor_command(vel_x_, vel_y_, ang_z_);
  }
  int sgn(double speed) {
    if (speed < 0) {
      return -1;
    } else if (speed > 0) {
      return 1;
    } else {
      return 0;
    }
  }

  void publish_motor_command(double velx, double vely, double angz) {
    auto joy_sig = move_control_msgs::msg::Teleop();

    if (agv_mode_ == 1 || agv_mode_ == 2 || agv_mode_ == 3) // MOVE MODE
    {
      joy_sig.mode = 0x01;
      joy_sig.linear_x = velx;
      joy_sig.linear_y = vely;
      joy_sig.angular_z = angz;
    }

    else {
      joy_sig.mode = 0x00;
      joy_sig.linear_x = 0;
      joy_sig.linear_y = 0;
      joy_sig.angular_z = 0;
    } // STOP MODE

    pub_joy_->publish(joy_sig);
  }

  double l_scale_x_ = 1.2, l_scale_y_ = 1.2, a_scale_z_ = 1.2, speed_scale_ = 1.0, slow_scale_ = 0.5;
  int move_mode_, agv_mode_, robot_arm_mode_;
  double vel_x_, vel_y_, ang_z_;
};

int main(int argc, char **argv) {
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<TeleopMotor>());
  rclcpp::shutdown();
  return 0;
}
