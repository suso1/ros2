from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package='joy_control',
            executable='joy_control',
            name = 'joy_control_node',
            output="screen",
            emulate_tty=True,
            parameters=[{"scale_linear_x": 1.0},
                        {"scale_linear_y": 1.0},
                        {"scale_ang_z": 1.0},
                        {"slow_scale": 0.5},
                        {"speed_scale": 1.2}]
        )
    ])
