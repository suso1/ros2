from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    
    return LaunchDescription([
       Node(
           package='usbdio',
           executable='usbdio_node',
           name = 'usbdio_node',
           output="screen",
           emulate_tty=True
       )
        
    ])
