#include "bdaqctrl.h"
#include "compatibility.h"
#include "geometry_msgs/msg/twist.hpp"
#include "move_control_msgs/msg/teleop.hpp"
#include "rclcpp/rclcpp.hpp"
#include "move_control_msgs/msg/dio_in.hpp"
#include "move_control_msgs/msg/dio_out.hpp"

#include <iostream>
#include <errno.h>
#include <fcntl.h>
#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <unistd.h>

#include <math.h>
#include <string>


using std::placeholders::_1;

using namespace Automation::BDaq;
typedef unsigned byte;
#define deviceDescription L"USB-4750,BID#0"

class usbdio_node : public rclcpp::Node {
public:
  usbdio_node() : Node("usb_dio") {
    pub_input_ = this->create_publisher<move_control_msgs::msg::DioIn>("/io_in", 10);
    sub_out_ = this->create_subscription<move_control_msgs::msg::DioOut>(
        "/io_out", 10, std::bind(&usbdio_node::io_out_callback, this, _1));
    sub_nav_ = this->create_subscription<geometry_msgs::msg::Twist>(
        "/cmd_vel", 10, std::bind(&usbdio_node::nav_ctrl_callback, this, _1));
    sub_joy_ = this->create_subscription<move_control_msgs::msg::Teleop>(
        "/cmd_vel_joy", 10,
        std::bind(&usbdio_node::joy_ctrl_callback, this, _1));

    for (int i = 0; i < 16; i++) {
      DOs_[i] = true;
      DIs_[i] = true;
    }

    ret = Success, ret2 = Success;
    instantDiCtrl = InstantDiCtrl::Create();
    instantDoCtrl = InstantDoCtrl::Create();

    DeviceInformation devInfo(deviceDescription);
    ret = instantDiCtrl->setSelectedDevice(devInfo);
    ret = instantDiCtrl->LoadProfile(profilePath);
    ret2 = instantDoCtrl->setSelectedDevice(devInfo);
    ret2 = instantDoCtrl->LoadProfile(profilePath);
  }

  ~usbdio_node() {
    instantDiCtrl->Dispose();
    instantDoCtrl->Dispose();
  }

  void spin() {
    while (rclcpp::ok()) {
      uint8 bufferforReading[64] = {
          0,
      };
      ret = instantDiCtrl->Read(start_port_, port_cnt_, bufferforReading);

      for (int i = 0; i < 8; i++) {
        DIs_[i] = (bool)((bufferforReading[start_port_] >> i) & 0x01);
      }
      for (int i = 0; i < 8; i++) {
        DOs_[i + 8] = (bool)((bufferforReading[start_port_ + 1] >> i) & 0x01);
      }

      emc_ = inv_bool(DIs_[in_emc_]);

      pw_sw_ = DIs_[in_pw_sw_];
      bump_ = DIs_[in_bump_];

      dio_pub_();

      if ((emc_) || (bump_)) {

        v_red_ = 1;
        v_gre_ = 0;
        v_yel_ = 0;
        v_buz_ = 1;

        v_mot_pw_ = 0;
        cnt_ = 0;

      } else {
        if (auto_run_) {

          v_yel_ = 0;
          v_buz_ = 0;
          v_red_ = 0;
          v_mot_pw_ = 1;

          if (cnt_ <= 100) {
            v_gre_ = 1;
          } else {
            v_gre_ = 0;
          }

          cnt_++;
          if (cnt_ >= 200)
            cnt_ = 0;

        } else {
          if (man_run_) {
            v_gre_ = 1;
            v_yel_ = 0;
            v_buz_ = 0;
            v_red_ = 0;
            cnt_ = 0;
            v_mot_pw_ = 1;
          } else {
            switch (emc_data) {
            case 0: {
              v_gre_ = 0;
              v_yel_ = 1;
              v_buz_ = 0;
              v_red_ = 0;
              cnt_ = 0;
              v_mot_pw_ = 1;
            } break;
            case 1: {
              v_gre_ = 0;
              v_buz_ = 1;
              v_red_ = 0;
              v_mot_pw_ = 1;
              if (cnt_ <= 100) {
                v_yel_ = 1;
              } else {
                v_yel_ = 0;
              }

              cnt_++;
              if (cnt_ >= 200)
                cnt_ = 0;
            } break;
            case 2: {
              v_gre_ = 0;
              v_yel_ = 0;
              v_buz_ = 1;
              v_mot_pw_ = 0;
              if (cnt_ <= 100) {
                v_red_ = 1;
              } else {
                v_red_ = 0;
              }

              cnt_++;
              if (cnt_ >= 200)
                cnt_ = 0;
            } break;
            }
          }
        }
      }

      ret2 = instantDoCtrl->WriteBit(start_port_, out_red_, v_red_);
      // CHK_RESULT(ret2);
      ret2 = instantDoCtrl->WriteBit(start_port_, out_buz_, v_buz_);
      // CHK_RESULT(ret2);
      ret2 = instantDoCtrl->WriteBit(start_port_, out_gre_, v_gre_);
      // CHK_RESULT(ret2);
      ret2 = instantDoCtrl->WriteBit(start_port_, out_yel_, v_yel_);
      // CHK_RESULT(ret2);
      ret2 = instantDoCtrl->WriteBit(start_port_, out_mot_pw_, v_mot_pw_);
    }

    ret2 = instantDoCtrl->WriteBit(start_port_, out_red_, 0);
    // CHK_RESULT(ret2);
    ret2 = instantDoCtrl->WriteBit(start_port_, out_buz_, 0);
    // CHK_RESULT(ret2);
    ret2 = instantDoCtrl->WriteBit(start_port_, out_gre_, 0);
    // CHK_RESULT(ret2);
    ret2 = instantDoCtrl->WriteBit(start_port_, out_yel_, 0);
    // CHK_RESULT(ret2);
    ret2 = instantDoCtrl->WriteBit(start_port_, out_mot_pw_, 0);
    if (BioFailed(ret)) {
      wchar_t enumString[256];
      AdxEnumToString(L"ErrorCode", (int32)ret, 256, enumString);
      printf("Some error occurred. And the last error code is 0x%X. [%ls]\n",
             ret, enumString);
    }
  }

private:
  void dio_pub_() {
    auto in_sig_ = move_control_msgs::msg::DioIn();
    in_sig_.bump = bump_;
    in_sig_.emc = emc_;
    in_sig_.powsw = pw_sw_;

    pub_input_->publish(in_sig_);
  }

  bool inv_bool(bool x)
  {
    if (x)
      return false;
    else
      return true;
  }

  void io_out_callback(const move_control_msgs::msg::DioOut::SharedPtr msg) {
    red_ = msg->red;
    yel_ = msg->yel;
    gre_ = msg->gre;
    buz_ = msg->buzz;
    if (red_)
      v_red_ = 1;
    else
      v_red_ = 0;

    if (gre_)
      v_gre_ = 1;
    else
      v_gre_ = 0;

    if (yel_)
      v_yel_ = 1;
    else
      v_yel_ = 0;

    if (buz_)
      v_buz_ = 1;
    else
      v_buz_ = 0;
  }

  void nav_ctrl_callback(const geometry_msgs::msg::Twist::SharedPtr msg)
  {
      if ((msg->linear.x == 0) && (msg->linear.y == 0) &&
            (msg->angular.z == 0))
          auto_run_ = false;
        else
          auto_run_ = true;
  }
  void joy_ctrl_callback(const move_control_msgs::msg::Teleop::SharedPtr msg)
  {
      if ((msg->linear_x == 0) && (msg->linear_y == 0) &&
            (msg->angular_z == 0))
          man_run_ = false;
        else
          man_run_ = true;
  }

  rclcpp::Publisher<move_control_msgs::msg::DioIn>::SharedPtr pub_input_;
  rclcpp::Subscription<move_control_msgs::msg::DioOut>::SharedPtr sub_out_;
  rclcpp::Subscription<move_control_msgs::msg::Teleop>::SharedPtr sub_joy_;
  rclcpp::Subscription<geometry_msgs::msg::Twist>::SharedPtr sub_nav_;
  const wchar_t *profilePath = L"../../profile/DemoDevice.xml";
  bool DIs_[16], DOs_[16];
  int32 start_port_ = 0;
  int32 port_cnt_ = 2;
  bool emc_ = false, pw_sw_ = true, red_ = false, gre_ = false, yel_ = false,
       bump_ = false, buz_ = false;

  int out_red_ = 0, out_gre_ = 2, out_yel_ = 3, out_mot_pw_ = 4, out_buz_ = 1;
  int in_emc_ = 0, in_bump_ = 1, in_pw_sw_ = 2;
  ErrorCode ret, ret2;
  InstantDiCtrl *instantDiCtrl;
  InstantDoCtrl *instantDoCtrl;

  int32_t cnt_ = 0;
  uint8_t emc_data = 0;

  bool auto_run_ = false, man_run_ = false;
  uint32 v_red_ = 0, v_gre_ = 0, v_yel_ = 0, v_buz_ = 0, v_mot_pw_ = 0;
};

int main(int argc, char *argv[]) {
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<usbdio_node>());
  rclcpp::shutdown();
  return 0;
}
