#include <chrono>
#include <functional>
#include <memory>
#include <string>

#include "driver_msg/msg/channel.hpp"
#include "driver_msg/msg/cmd.hpp"
#include "driver_msg/msg/status.hpp"
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include <serial/serial.h>
#include <sstream>

using namespace std::chrono_literals;
using std::placeholders::_1;

class WheelDriver : public rclcpp::Node {
public:
  WheelDriver();

private:
  void cmdvel_callback(const driver_msg::msg::Cmd::SharedPtr msg);
  void timer_callback();

  void driver_configure();
  void data_query();
  void data_parsing();

  void encoder_publish();
  void speed_publish();
  void status_publish();

  void send_cmd();

  int run();

  rclcpp::Subscription<driver_msg::msg::Cmd>::SharedPtr sub_cmd_;
  rclcpp::Publisher<driver_msg::msg::Channel>::SharedPtr pub_encoder_;
  rclcpp::Publisher<driver_msg::msg::Channel>::SharedPtr pub_speed_;
  rclcpp::Publisher<driver_msg::msg::Status>::SharedPtr pub_status_;
  rclcpp::TimerBase::SharedPtr timer_;

  serial::Serial controller_;
  static int32_t to_rpm(int32_t x) { return int32_t(x); }

  uint32_t data_idx_ = 0;
  char data_buf_[24] = {
      0,
  };
  int8_t mode_ = 1;
  float left_spd_ = 0.0, right_spd_ = 0.0;
  int32_t left_enc_ = 0, right_enc_ = 0;
  int32_t left_rpm_ = 0, right_rpm_ = 0;

  // settings

  std::string cmdvel_topic_ = "/CMD";
  std::string port_ = "/dev/ttyUSB0";
  int baud_ = 115200;
  bool open_loop_ = false;
  int encoder_ppr_ = 300;
  int encoder_cpr_ = 12000;
  double max_rpm_ = 2000;
  int data_encoder_toss_ = 0;

  int dir_ = 0;
  int bat_vol_ = 0, current_ = 0, internal_vol_ = 0;
  int fault_flag_ = 0, hall_status_ = 0, status_flag_ = 0;
  bool connect_ = false;
  int cnt_ = 0;
};

WheelDriver::WheelDriver() : Node("wheel_driver") {
  sub_cmd_ = this->create_subscription<driver_msg::msg::Cmd>(
      cmdvel_topic_, 10, std::bind(&WheelDriver::cmdvel_callback, this, _1));
  pub_encoder_ =
      this->create_publisher<driver_msg::msg::Channel>("encoder", 10);
  pub_speed_ =
      this->create_publisher<driver_msg::msg::Channel>("speed", 10);
  pub_status_ =
      this->create_publisher<driver_msg::msg::Status>("status", 10);
  timer_ = this->create_wall_timer(
      1ms, std::bind(&WheelDriver::timer_callback, this));

  this->declare_parameter<std::string>("cmdvel_topic", "front/CMD");
  this->get_parameter("cmdvel_topic", cmdvel_topic_);
  RCLCPP_INFO(this->get_logger(), "cmdvel_topic =  '%s'",
              cmdvel_topic_.c_str());

  this->declare_parameter<int>("encoder_cpr", 12000);
  this->get_parameter("encoder_cpr", encoder_cpr_);
  RCLCPP_INFO(this->get_logger(), "encoder_cpr =  '%d'", encoder_cpr_);

  this->declare_parameter<int>("encoder_ppr", 3000);
  this->get_parameter("encoder_ppr", encoder_ppr_);
  RCLCPP_INFO(this->get_logger(), "encoder_ppr =  '%d'", encoder_ppr_);

  this->declare_parameter<std::string>("port", "/dev/ttyUSB2");
  this->get_parameter("port", port_);
  RCLCPP_INFO(this->get_logger(), "port =  '%s'", port_.c_str());

  this->declare_parameter<int>("baud", 115200);
  this->get_parameter("baud", baud_);
  RCLCPP_INFO(this->get_logger(), "baudrate =  '%d'", baud_);

  this->declare_parameter<double>("max_rpm", 2000.0);
  this->get_parameter("max_rpm", max_rpm_);
  RCLCPP_INFO(this->get_logger(), "max_rpm =  '%f'", max_rpm_);

  this->declare_parameter<int>("dir", 0);
  this->get_parameter("dir", dir_);
  RCLCPP_INFO(this->get_logger(), "dir =  '%d'", dir_);

  this->declare_parameter<bool>("open_loop", 0);
  this->get_parameter("open_loop", open_loop_);
  RCLCPP_INFO(this->get_logger(), "openloop =  '%d'", open_loop_);

  run();
}

void WheelDriver::cmdvel_callback(const driver_msg::msg::Cmd::SharedPtr msg)  {
  RCLCPP_INFO(this->get_logger(), "Received command");
  mode_ = msg->mode;
  right_spd_ = msg->right;
  left_spd_ = msg->left;
  RCLCPP_INFO(this->get_logger(), "mode: '%d', right: '%f', left: '%f'", mode_,
              right_spd_, left_spd_);

  send_cmd();
}
void WheelDriver::timer_callback()
{
  data_parsing();
}
void WheelDriver::send_cmd() {
  // RCLCPP_INFO(this->get_logger(), "Enter loop ");
  if (connect_) {
    // RCLCPP_INFO(this->get_logger(), "Connnect ok ...");
    if (mode_ == 1) {
      // RCLCPP_INFO(this->get_logger(), "Mode run ... ");
      if (open_loop_) {
        RCLCPP_INFO(this->get_logger(), "Open loop ");
        // motor power (scale 0-1000)

        int32_t right_power = static_cast<int>(right_spd_ * 1);
        int32_t left_power = static_cast<int>(left_spd_ * 1);

        std::stringstream right_cmd;
        std::stringstream left_cmd;

        right_cmd << "!G 1 " << right_power << "\r";
        left_cmd << "!G 2 " << left_power << "\r";

        controller_.write(right_cmd.str());
        controller_.write(left_cmd.str());
        controller_.flush();
      } else {
        // motor speed (rpm)

        int32_t right_rpm = static_cast<int>(right_spd_ * 1);
        int32_t left_rpm = static_cast<int>(left_spd_ * 1);

        // RCLCPP_INFO(this->get_logger(), "Control speed = 500 rpm ");

        //int32_t right_rpm = static_cast<int>(500 * 1);
        //int32_t left_rpm = static_cast<int>(500 * 1);

        std::stringstream right_cmd;
        std::stringstream left_cmd;

        right_cmd << "!S 1 " << right_rpm << "\r";
        left_cmd << "!S 2 " << left_rpm << "\r";

        controller_.write(right_cmd.str());
        controller_.write(left_cmd.str());
        controller_.flush();
      }
    } else {
      RCLCPP_INFO(this->get_logger(), "Other  options ");
      int32_t right_rpm = 0;
      int32_t left_rpm = 0;

      std::stringstream right_cmd;
      std::stringstream left_cmd;

      right_cmd << "!S 1 " << right_rpm << "\r";
      left_cmd << "!S 2 " << left_rpm << "\r";

      controller_.write(right_cmd.str());
      controller_.write(left_cmd.str());
      controller_.flush();
    }
  }
}

void WheelDriver::driver_configure() {
  // stop motors
  RCLCPP_INFO(this->get_logger(), "Setup for motor driver \n");
  controller_.write("!G 1 0\r");
  controller_.write("!G 2 0\r");
  controller_.write("!S 1 0\r");
  controller_.write("!S 2 0\r");
  controller_.flush();

  // disable echo
  controller_.write("^ECHOF 1\r");
  controller_.flush();

  // enable watchdog timer (1000 ms)
  controller_.write("^RWD 250\r");

  // set motor operating mode (1 for closed-loop speed)
  if (open_loop_) {
    controller_.write("^MMOD 1 0\r");
  } else {
    controller_.write("^MMOD 1 1\r");
    RCLCPP_INFO(this->get_logger(), "Closed-loop");
  }

  // set motor amps limit (20 A * 10)
  RCLCPP_INFO(this->get_logger(), "Amp lim = 500");
  controller_.write("^ALIM 1 500\r");
  controller_.write("^ALIM 2 500\r");

  // set max speed (rpm) for relative speed cmds
  //  controller_.write("^MXRPM 1 82\r");
  //  controller_.write("^MXRPM 2 82\r");
  RCLCPP_INFO(this->get_logger(), "Max RPM = 2000");
  controller_.write("^MXRPM 1 2000\r");
  controller_.write("^MXRPM 2 2000\r");

  // set max acceleration rate (500 rpm/s * 10)
  //  controller_.write("^MAC 1 2000\r");
  //  controller_.write("^MAC 2 2000\r");
  RCLCPP_INFO(this->get_logger(), "Acc = 5000");
  controller_.write("^MAC 1 5000\r");
  controller_.write("^MAC 2 5000\r");

  // set max deceleration rate (2000 rpm/s * 10)
  RCLCPP_INFO(this->get_logger(), "Deacc = 20000");
  controller_.write("^MDEC 1 20000\r");
  controller_.write("^MDEC 2 20000\r");

  // set number of poles
  RCLCPP_INFO(this->get_logger(), "No of pole pairs = 2");
  controller_.write("^BPOL 1 2\r");
  controller_.write("^BPOL 2 2\r");

  // set PID parameters (gain * 10)
  RCLCPP_INFO(this->get_logger(), "Kp = 0, Ki = 50, Kd = 0");
  controller_.write("^KP 1 0\r");
  controller_.write("^KP 2 0\r");
  controller_.write("^KI 1 50\r");
  controller_.write("^KI 2 50\r");
  controller_.write("^KD 1 0\r");
  controller_.write("^KD 2 0\r");

  // set encoder mode (18 for feedback on motor1, 34 for feedback on motor2)
  RCLCPP_INFO(this->get_logger(), "Set encoder feedback");
  controller_.write("^EMOD 1 18\r");
  controller_.write("^EMOD 2 34\r");

  // set motor direction
  if (dir_ == 1) {
    controller_.write("^MDIR 1 0\r");
    controller_.write("^MDIR 2 1\r");
    RCLCPP_INFO(this->get_logger(), "Dir = 1");
  } else {
    controller_.write("^MDIR 1 1\r");
    controller_.write("^MDIR 2 0\r");
    RCLCPP_INFO(this->get_logger(), "Dir = 0");
  }

  // set encoder counts (ppr)
  std::stringstream enc_cmd1_, enc_cmd2_;
  enc_cmd1_ << "^EPPR 1 " << encoder_ppr_ << "\r";
  controller_.write(enc_cmd1_.str());
  enc_cmd2_ << "^EPPR 2 " << encoder_ppr_ << "\r";
  controller_.write(enc_cmd2_.str());
  controller_.flush();
}

void WheelDriver::data_query() {
  controller_.write("# C_?C_?S_?BA_?V_?FF_?FS_?HS_# 20\r");
  // controller_.write("# C_?C_?S_# 20\r");
  controller_.flush();
}

void WheelDriver::encoder_publish() {
  auto encoder_value = driver_msg::msg::Channel();
  encoder_value.left = left_enc_;
  encoder_value.right = right_enc_;

  pub_encoder_->publish(encoder_value);
}

void WheelDriver::speed_publish() {
  auto speed_value = driver_msg::msg::Channel();
  speed_value.left = left_rpm_;
  speed_value.right = right_rpm_;

  pub_speed_->publish(speed_value);
}

void WheelDriver::data_parsing() {
  if (controller_.available()) {
    char ch = 0;
    if (controller_.read((uint8_t *)&ch, 1) == 0)
      return;
    if (ch == '\r') {
      data_buf_[data_idx_] = 0;

      // Absolute encoder value C

      if (data_buf_[0] == 'C' && data_buf_[1] == '=') {
        uint32_t delim;
        for (delim = 2; delim < data_idx_; delim++) {
          if (data_encoder_toss_ > 0) {
            --data_encoder_toss_;
            break;
          }
          if (data_buf_[delim] == ':') {
            data_buf_[delim] = 0;
            right_enc_ = (int32_t)strtol(data_buf_ + 2, NULL, 10);
            left_enc_ = (int32_t)strtol(data_buf_ + delim + 1, NULL, 10);
            encoder_publish();
            break;
          }
        }
      }
      // S= is speed in RPM
      if (data_buf_[0] == 'S' && data_buf_[1] == '=') {
        uint32_t delim;
        for (delim = 2; delim < data_idx_; delim++) {
          if (data_buf_[delim] == ':') {
            data_buf_[delim] = 0;
            right_rpm_ = (int32_t)strtol(data_buf_ + 2, NULL, 10);
            left_rpm_ = (int32_t)strtol(data_buf_ + delim + 1, NULL, 10);
            speed_publish();
            break;
          }
        }
      }

      // Voltage
      if (data_buf_[0] == 'V' && data_buf_[1] == '=') {
        uint32_t delim;
        for (delim = 2; delim < data_idx_; delim++) {
          if (data_buf_[delim] == ':') {
            data_buf_[delim] = 0;
            internal_vol_ = (int32_t)strtol(data_buf_ + 2, NULL, 10);
            bat_vol_ = (int32_t)strtol(data_buf_ + delim + 1, NULL, 10);
            break;
          }
        }
      }

      // Current
      if (data_buf_[0] == 'B' && data_buf_[1] == 'A' && data_buf_[2] == '=') {
        uint32_t delim;
        for (delim = 3; delim < data_idx_; delim++) {
          if (data_buf_[delim] == ':') {
            data_buf_[delim] = 0;
            current_ = (int32_t)strtol(data_buf_ + 3, NULL, 10);
            break;
          }
        }
      }
      // Fault
      if (data_buf_[0] == 'F' && data_buf_[1] == 'F' && data_buf_[2] == '=') {
        fault_flag_ = (int32_t)strtol(data_buf_ + 3, NULL, 10);
      }
      // status
      if (data_buf_[0] == 'F' && data_buf_[1] == 'S' && data_buf_[2] == '=') {
        status_flag_ = (int32_t)strtol(data_buf_ + 3, NULL, 10);
      }

      // Hall sensor
      if (data_buf_[0] == 'H' && data_buf_[1] == 'S' && data_buf_[2] == '=') {
        uint32_t delim;
        for (delim = 3; delim < data_idx_; delim++) {
          if (data_buf_[delim] == ':') {
            data_buf_[delim] = 0;
            hall_status_ = (int32_t)strtol(data_buf_ + 3, NULL, 10);
            break;
          }
        }
      }

      status_publish();

      data_idx_ = 0;
    } else if (data_idx_ < (sizeof(data_buf_) - 1)) {
      data_buf_[data_idx_++] = ch;
    }
  }
}

void WheelDriver::status_publish() {
  auto status_msgs = driver_msg::msg::Status();
  status_msgs.status = status_flag_;
  status_msgs.fault = fault_flag_;
  status_msgs.bat_vol = bat_vol_;
  status_msgs.internal_vol = internal_vol_;
  status_msgs.cur = current_;
  status_msgs.hall = hall_status_;
  pub_status_->publish(status_msgs);
}

int WheelDriver::run() {

  serial::Timeout timeout = serial::Timeout::simpleTimeout(1000);
  controller_.setPort(port_);
  controller_.setBaudrate(baud_);
  controller_.setTimeout(timeout);

  while (rclcpp::ok()) {
    try {
      controller_.open();
      if (controller_.isOpen()) {
        RCLCPP_INFO(this->get_logger(), "Successfully opened serial port \n");
        connect_ = true;
        break;
      }
    } catch (serial::IOException e) {
      // RCLCPP_INFO(this->get_logger(), "Connection failed \n");
      connect_ = false;
    }
    RCLCPP_INFO(this->get_logger(), "Cannot open serial port \n");
    rclcpp::sleep_for(1s);
  }
  if (connect_) {
    driver_configure();
    data_query();
  }


//  while (rclcpp::ok()) {
//    if (connect_) {
//      data_parsing();

//      if(cnt_>=10)
//      {
//        timer_callback();
//        cnt_ = 0;
//      }
//      else
//        cnt_ ++;

//    }
    // rclcpp::sleep_for(10ms);
  //}

 // else (controller_.isOpen()){
  //  controller_.close();

  return 0;
}

int main(int argc, char *argv[]) {
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<WheelDriver>());
  rclcpp::shutdown();
  return 0;
}