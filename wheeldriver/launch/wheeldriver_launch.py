from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    front_remappings = [
    ('speed', 'front/speed'),
    ('encoder', 'front/encoder'),
    ('status', 'front/status')]
    rear_remappings = [
    ('speed', 'rear/speed'),
    ('encoder', 'rear/encoder'),
    ('status', 'rear/status')]
    return LaunchDescription([
        Node(
            package='wheeldriver',
            executable='wheeldriver',
            name = 'front_wheel_driver',
            output="screen",
            emulate_tty=True,
            parameters=[{"encoder_cpr": 12000},
                        {"encoder_ppr": 3000},
                        {"cmdvel_topic": "/front/CMD"},
                        {"port": "/dev/ttyUSB2"},
                        {"baud": 115200},
                        {"max_rpm": 2000.0},
                        {"open_loop": False},
                        {"dir": 1}],
            remappings = front_remappings
        ),
        # Node(
        #     package='wheeldriver',
        #     executable='wheeldriver',
        #     name = 'rear_wheel_driver',
        #     output="screen",
        #     emulate_tty=True,
        #     parameters=[{"encoder_cpr": 12000},
        #                 {"encoder_ppr": 3000},
        #                 {"cmdvel_topic": "/rear/CMD"},
        #                 {"port": "/dev/ttyUSB2"},
        #                 {"baud": 115200},
        #                 {"max_rpm": 2000.0},
        #                 {"open_loop": False},
        #                 {"dir": 1}],
        #     remappings = rear_remappings
        # ),
    ])
