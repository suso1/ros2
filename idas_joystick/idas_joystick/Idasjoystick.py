import serial
import threading
from numba import jit
import rclpy
from rclpy.node import Node
from std_msgs.msg import String
from driver_msg.msg import IdasData



class Idasjoystick(Node):

    def __init__(self):
        super().__init__('Idas_joystick')
        PORT = "/dev/ttyUSB1"
        baudrate = 115200
        self.ser = serial.Serial(port=PORT, baudrate=baudrate)
        print(f'open parameter: {PORT},{baudrate}')
        
        self.valueInit()
        self.publisher_ = self.create_publisher(IdasData, '/idas_data', 10)
        timer_period = 0.1  # seconds
        self.readSerial()
        self.timer = self.create_timer(timer_period, self.timer_callback)

    def readSerial(self):
        thread = threading.Thread(target=self.idasValue)
        thread.start()

    def idasValue(self):
        while True:
            if self.ser.readable():
                self.res = self.ser.readline().decode('utf8')
                self.parsing()
            elif self.sv[14] != 40:
                self.valueReset()
            else:
                print("Serial port can't open")
                self.valueReset()
        
    def valueInit(self):
        self.an1 = 0.0
        self.an2 = 0.0
        self.an3 = 0.0
        self.an4 = 0.0
        self.an5 = 0.0
        self.an6 = 0.0
        self.an7 = 0.0
        self.an8 = 0.0

        self.ky1 = 0
        self.ky2 = 0
        self.ky3 = 0
        self.ky4 = 0
        self.da1 = 0
        self.da2 = 0
        self.da3 = 0
        self.da4 = 0

    @jit
    def valueReset(self):
        i = 0
        j = 0
        k = 0
            
        for i in range(8):
            self.an[i] = 0.0
            
        for j in range(4):
            self.ky[j] = 0.0

        for k in range(4):
            self.da[k] = 0.0
    
    def parsing(self):
        self.value = str(self.res[1:53])
        #splited value
        self.sv = self.value.split(',')
        print(self.sv)

    def timer_callback(self):
        #message = can.Message(arbitration_id=1E4, is_extended_id=True,)
        # for msg in self.bus:
        if self.res[0:1] == '#':
            #print("hello i found #")
            self.an1 = float(1.0 - int((self.sv[0]), 16)/127)
            self.an2 = float(1.0 - int((self.sv[1]), 16)/127)
            self.an3 = float(1.0 - int((self.sv[2]), 16)/127)
            self.an4 = float(1.0 - int((self.sv[3]), 16)/127)
            self.an5 = float(1.0 - int((self.sv[4]), 16)/127)
            self.an6 = float(1.0 - int((self.sv[5]), 16)/127)
            self.an7 = float(1.0 - int((self.sv[6]), 16)/127)
            self.an8 = float(1.0 - int((self.sv[7]), 16)/127)
            self.ky1 = int(self.sv[8], 16)
            self.ky2 = int(self.sv[9], 16)
            self.ky3 = int(self.sv[10], 16)
            self.ky4 = int(self.sv[11], 16)
            self.da1 = int(self.sv[12], 16)
            self.da2 = int(self.sv[13], 16)
            self.da3 = int(self.sv[14], 16)
            self.da4 = int(self.sv[15], 16)

            idas_msg_ = IdasData()
            idas_msg_.an1 = self.an1
            idas_msg_.an2 = self.an2
            idas_msg_.an3 = self.an3
            idas_msg_.an4 = self.an4
            idas_msg_.an5 = self.an5
            idas_msg_.an6 = self.an6
            idas_msg_.an7 = self.an7
            idas_msg_.an8 = self.an8
            idas_msg_.ky1 = self.ky1
            idas_msg_.ky2 = self.ky2
            idas_msg_.ky3 = self.ky3
            idas_msg_.ky4 = self.ky4
            idas_msg_.da1 = self.da1
            idas_msg_.da2 = self.da2
            idas_msg_.da3 = self.da3
            idas_msg_.da4 = self.da4
            self.publisher_.publish(idas_msg_)
            #print(self.ky1)

def main(args=None):
    rclpy.init(args=args)
    Idas_joystick = Idasjoystick()
    rclpy.spin(Idas_joystick)
    Idas_joystick.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
