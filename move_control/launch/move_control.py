from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    front_remappings = [
    ('/speed', '/front/speed'),
    ('/encoder', '/front/encoder'),
    ('/status', '/front/status'),
    ('/CMD','/front/CMD')
    ]
    rear_remappings = [
    ('/speed', '/rear/speed'),
    ('/encoder', '/rear/encoder'),
    ('/status', '/rear/status'),
    ('/CMD', '/rear/CMD')
    ]
    return LaunchDescription([
        Node(
            package='move_control',
            executable='move_control',
            name = 'move_control_node',
            output="screen",
            emulate_tty=True,
            parameters=[{"agv_len": 1.44},
                        {"agv_width": 1.24},
                        {"wheel_rad": 0.18},
                        {"gear_ratio": 40.0}]
        ),
        Node(
            package='wheeldriver',
            executable='wheeldriver',
            name = 'front_wheel_driver',
            output="screen",
            emulate_tty=True,
            parameters=[{"encoder_cpr": 12000},
                        {"encoder_ppr": 3000},
                        {"cmdvel_topic": "/front/CMD"},
                        {"port": "/dev/ttyUSB2"},
                        {"baud": 115200},
                        {"max_rpm": 2000.0},
                        {"open_loop": False},
                        {"dir": 1}],
            remappings=front_remappings
        ),

        Node(
            package='wheeldriver',
            executable='wheeldriver',
            name = 'rear_wheel_driver',
            output="screen",
            emulate_tty=True,
            parameters=[{"encoder_cpr": 12000},
                        {"encoder_ppr": 3000},
                        {"cmdvel_topic": "/rear/CMD"},
                        {"port": "/dev/ttyUSB3"},
                        {"baud": 115200},
                        {"max_rpm": 2000.0},
                        {"open_loop": False},
                        {"dir": 1}],
            remappings=rear_remappings
        ),
        Node(
            package='joy_control',
            executable='joy_control',
            name = 'joy_control_node',
            output="screen",
            emulate_tty=True,
            parameters=[{"scale_linear_x": 1.5},
                        {"scale_linear_y": 1.5},
                        {"scale_ang_z": 1.5},
                        {"slow_scale": 0.5},
                        {"speed_scale": 0.6}]
        ),
       Node(
           package='idas_joystick',
           executable='Idasjoystick',
           name = 'idas_joystick',
           output="screen",
           emulate_tty=True
       )
        
    ])
