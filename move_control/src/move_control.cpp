#include <chrono>
#include <functional>
#include <math.h>
#include <memory>
#include <string>

#include "geometry_msgs/msg/twist.hpp"
#include "move_control_msgs/msg/teleop.hpp"
#include "move_control_msgs/msg/wheel_data.hpp"

#include "driver_msg/msg/channel.hpp"
#include "driver_msg/msg/cmd.hpp"

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

using namespace std;
using std::placeholders::_1;

class MoveControl : public rclcpp::Node {
  enum {
    FRONT_RIGHT = 0,
    REAR_RIGHT = 1,
    REAR_LEFT = 2,
    FRONT_LEFT = 3,
    FRONT = 0,
    REAR = 1
  };

    struct Agv
    {
      float len = 1.44f;
      float width = 1.24f;
      float wheel_rad = 0.18f;
      float wheel_width = 2.1f;
      float gear_ratio = 1.0f;
    };

    struct Scale
    {
      float x = 1.0f;
      float y = 1.0f;
      float th = 1.0f;
    };

    struct MotorCmd
    {
      double front_left = 0;
      double front_right = 0;
      double rear_left = 0;
      double rear_right = 0;
    };


    struct Mode
    {
      int8_t  stop = 0;
      int8_t  run = 1;
    };

  double set_speed_[4] = {}; // init 0
  double measure_speed_[4] = {}; //init 0
  bool emc_stop_ = false;

public:
  MoveControl() : Node("MoveControl") {
    pub_wheel_speed_ =
        this->create_publisher<move_control_msgs::msg::WheelData>("/wheeldata",
                                                                  10);
    pub_move_cmd_[FRONT] =
        this->create_publisher<driver_msg::msg::Cmd>("/front/CMD", 10);
    pub_move_cmd_[REAR] =
        this->create_publisher<driver_msg::msg::Cmd>("/rear/CMD", 10);

    sub_joy_ = this->create_subscription<move_control_msgs::msg::Teleop>(
        "/cmd_vel_joy", 10,
        std::bind(&MoveControl::joystick_control_callback, this, _1));

    sub_auto_ = this->create_subscription<geometry_msgs::msg::Twist>(
        "/cmd_vel", 10,
        std::bind(&MoveControl::auto_control_callback, this, _1));

    sub_agv_spd_[FRONT] = this->create_subscription<driver_msg::msg::Channel>(
        "/front/speed", 10,
        std::bind(&MoveControl::front_speed_callback, this, _1));

    sub_agv_spd_[REAR] = this->create_subscription<driver_msg::msg::Channel>(
        "/rear/speed", 10,
        std::bind(&MoveControl::rear_speed_callback, this, _1));

    this->declare_parameter<double>("len", 1.4);
    this->get_parameter("len", agv_.len);
    RCLCPP_INFO(this->get_logger(), "len =  '%f'", agv_.len);

    this->declare_parameter<double>("width", 1.24);
    this->get_parameter("width", agv_.width);
    RCLCPP_INFO(this->get_logger(), "width =  '%f'", agv_.width);

    this->declare_parameter<double>("wheel_rad", 0.18);
    this->get_parameter("wheel_rad", agv_.wheel_rad);
    RCLCPP_INFO(this->get_logger(), "wheel_rad =  '%f'", agv_.wheel_rad);

    this->declare_parameter<double>("gear_ratio", 40.0);
    this->get_parameter("gear_ratio", agv_.gear_ratio);
    RCLCPP_INFO(this->get_logger(), "gear_ratio =  '%f'", agv_.gear_ratio);

    // spin();
  }
  /*
    int spin() {
      while (rclcpp::ok()) {
        publish_cmd();
        rclcpp::sleep_for(10ms);
      }
    }
    */

private:
  rclcpp::Publisher<move_control_msgs::msg::WheelData>::SharedPtr
      pub_wheel_speed_;
  rclcpp::Publisher<driver_msg::msg::Cmd>::SharedPtr pub_move_cmd_[2];

  rclcpp::Subscription<move_control_msgs::msg::Teleop>::SharedPtr sub_joy_;
  rclcpp::Subscription<geometry_msgs::msg::Twist>::SharedPtr sub_auto_;
  rclcpp::Subscription<driver_msg::msg::Channel>::SharedPtr sub_agv_spd_[2];

  Mode mode_;
  MotorCmd nav_cmd_, joy_cmd_;
  Scale scale_;
  Agv agv_;

  void
  front_speed_callback(const driver_msg::msg::Channel::SharedPtr speed_val_) {
    measure_speed_[FRONT_RIGHT] = speed_val_->right;
    measure_speed_[FRONT_LEFT] = speed_val_->left;
  }

  void
  rear_speed_callback(const driver_msg::msg::Channel::SharedPtr speed_val_) {
    measure_speed_[REAR_RIGHT] = speed_val_->right;
    measure_speed_[REAR_LEFT] = speed_val_->left;
  }

  void
  auto_control_callback(const geometry_msgs::msg::Twist::SharedPtr motorCmd_) {
    RCLCPP_INFO(this->get_logger(), "auto_control command recevied");
    
    double linear_x = motorCmd_->linear.x;
    double linear_y = motorCmd_->linear.y;
    double angular_z = motorCmd_->angular.z;

    RCLCPP_INFO(this->get_logger(),
                    "vel_x: '%f', vel_y: '%f', ang_z: '%f'",
                    linear_x, linear_y, angular_z);

    nav_cmd_.front_right = 60 / (2 * M_PI * agv_.wheel_rad) *
                          ((1) * linear_x * scale_.x + (-1) * linear_y * scale_.y -
                          (agv_.len + agv_.width) * scale_.th * angular_z / 2) * agv_.gear_ratio;

    nav_cmd_.front_left = 60 / (2 * M_PI * agv_.wheel_rad) *
                          (1 * linear_x * scale_.x + (1) * linear_y * scale_.y +
                          (agv_.len + agv_.width) * scale_.th * angular_z / 2) * agv_.gear_ratio;

    nav_cmd_.rear_left = 60 / (2 * M_PI * agv_.wheel_rad) *
                        ((1) * linear_x * scale_.x + (-1) * linear_y * scale_.y +
                        (agv_.len + agv_.width) * angular_z / 2 * scale_.th) * agv_.gear_ratio;

    nav_cmd_.rear_right = 60 / (2 * M_PI * agv_.wheel_rad) *
                        (1 * linear_x * scale_.x + 1 * linear_y * scale_.y -
                        (agv_.len + agv_.width) * scale_.th * angular_z / 2) * agv_.gear_ratio;
    publish_cmd();
    return;
  }

  void joystick_control_callback(
    const move_control_msgs::msg::Teleop::SharedPtr motor_cmd_) {
      if (motor_cmd_->mode == 1) {
        double linear_x = motor_cmd_->linear_x * 1.0;
        double linear_y = -motor_cmd_->linear_y * 1.0;
        double angular_z = -motor_cmd_->angular_z * 1.0;

        RCLCPP_INFO(this->get_logger(), "Joystick command:");
        RCLCPP_INFO(this->get_logger(),
                    "mode: '%d', vel_x: '%f', vel_y: '%f', ang_z: '%f'",
                    motor_cmd_->mode, linear_x, linear_y, angular_z);

        joy_cmd_.front_right = 60 / (2 * M_PI * agv_.wheel_rad) *
                              ((1) * linear_x + (-1) * linear_y -
                              (agv_.len + agv_.width) * scale_.th * angular_z / 2) *
                              agv_.gear_ratio;
        joy_cmd_.front_left = 60 / (2 * M_PI * agv_.wheel_rad) *
                              (1 * linear_x + (1) * linear_y +
                              (agv_.len + agv_.width) * scale_.th * angular_z / 2) *
                              agv_.gear_ratio;
        joy_cmd_.rear_left = 60 / (2 * M_PI * agv_.wheel_rad) *
                              ((1) * linear_x + (-1) * linear_y +
                              (agv_.len + agv_.width) * scale_.th * angular_z / 2) *
                              agv_.gear_ratio;
        joy_cmd_.rear_right = 60 / (2 * M_PI * agv_.wheel_rad) *
                              (1 * linear_x + (1) * linear_y -
                              (agv_.len + agv_.width) * scale_.th * angular_z / 2) *
                              agv_.gear_ratio;

        nav_cmd_.front_right = 0;
        nav_cmd_.front_right = 0;
        nav_cmd_.rear_left = 0;
        nav_cmd_.rear_right = 0;
    }
    else {
      joy_cmd_.front_right = 0;
      joy_cmd_.front_left = 0;
      joy_cmd_.rear_left = 0;
      joy_cmd_.rear_right = 0;
    }
    publish_cmd();
  }

  void publish_cmd() {
    set_speed_[FRONT_LEFT] = joy_cmd_.front_left + nav_cmd_.front_left;
    set_speed_[FRONT_RIGHT] = joy_cmd_.front_right + nav_cmd_.front_left;
    set_speed_[REAR_LEFT] = joy_cmd_.rear_left + nav_cmd_.rear_left;
    set_speed_[REAR_RIGHT] = joy_cmd_.rear_right + nav_cmd_.rear_right;

    if (emc_stop_) {
      set_stop_cmd();
    } else {
        if ((set_speed_[FRONT_LEFT] == 0) && (set_speed_[FRONT_RIGHT] == 0) &&
            (set_speed_[REAR_LEFT] == 0) && (set_speed_[REAR_RIGHT] == 0)) 
        {
          set_stop_cmd();
        }

        else
        {
          set_move_cmd();
        }
    }
  }

  bool set_move_cmd() {
    // driver_msg::msg::Cmd driver_msg_[2];
    auto driver_msg_front_ = driver_msg::msg::Cmd();
    driver_msg_front_.mode = mode_.run;
    driver_msg_front_.left = set_speed_[FRONT_LEFT];
    driver_msg_front_.right = set_speed_[FRONT_RIGHT];

    auto driver_msg_rear_ = driver_msg::msg::Cmd();

    driver_msg_rear_.mode = mode_.run;
    driver_msg_rear_.left = set_speed_[REAR_LEFT];
    driver_msg_rear_.right = set_speed_[REAR_RIGHT];

    pub_move_cmd_[FRONT]->publish(driver_msg_front_);
    pub_move_cmd_[REAR]->publish(driver_msg_rear_);
    return 0;
  }

  bool set_stop_cmd() {
    auto driver_msg_front_ = driver_msg::msg::Cmd();
    driver_msg_front_.mode = mode_.stop;
    driver_msg_front_.left = 0.0;
    driver_msg_front_.right = 0.0;

    auto driver_msg_rear_ = driver_msg::msg::Cmd();

    driver_msg_rear_.mode = mode_.stop;
    driver_msg_rear_.left = 0.0;
    driver_msg_rear_.right = 0.0;

    pub_move_cmd_[FRONT]->publish(driver_msg_front_);
    pub_move_cmd_[REAR]->publish(driver_msg_rear_);
    return 0;
  }
};

int main(int argc, char *argv[]) {
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<MoveControl>());
  rclcpp::shutdown();
  return 0;
}
